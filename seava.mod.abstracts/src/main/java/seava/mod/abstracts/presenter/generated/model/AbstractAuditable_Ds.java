/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.abstracts.presenter.generated.model;

import java.util.Date;
import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.presenter.model.AbstractDsModel;

public class AbstractAuditable_Ds<E> extends AbstractDsModel<E> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "AbstractAuditable_Ds";
	
	public static final String f_id = "id";
	public static final String f_clientId = "clientId";
	public static final String f_notes = "notes";
	public static final String f_createdAt = "createdAt";
	public static final String f_modifiedAt = "modifiedAt";
	public static final String f_createdBy = "createdBy";
	public static final String f_modifiedBy = "modifiedBy";
	public static final String f_version = "version";
	public static final String f_refid = "refid";
	public static final String f_entityAlias = "entityAlias";
	public static final String f_entityFqn = "entityFqn";
	
	@DsField
	private String id;
	
	@DsField
	private String clientId;
	
	@DsField
	private String notes;
	
	@DsField
	private Date createdAt;
	
	@DsField
	private Date modifiedAt;
	
	@DsField
	private String createdBy;
	
	@DsField
	private String modifiedBy;
	
	@DsField
	private Long version;
	
	@DsField
	private String refid;
	
	@DsField(fetch=false)
	private String entityAlias;
	
	@DsField(fetch=false)
	private String entityFqn;
	
	public AbstractAuditable_Ds() {
		super();
	}
	
	public AbstractAuditable_Ds(E e) {
		super(e);
	}
	
	public String getId() {
	  return this.id;
	}
	
	public void setId(String id) {
	  this.id = id;
	}
	
	public String getClientId() {
	  return this.clientId;
	}
	
	public void setClientId(String clientId) {
	  this.clientId = clientId;
	}
	
	public String getNotes() {
	  return this.notes;
	}
	
	public void setNotes(String notes) {
	  this.notes = notes;
	}
	
	public Date getCreatedAt() {
	  return this.createdAt;
	}
	
	public void setCreatedAt(Date createdAt) {
	  this.createdAt = createdAt;
	}
	
	public Date getModifiedAt() {
	  return this.modifiedAt;
	}
	
	public void setModifiedAt(Date modifiedAt) {
	  this.modifiedAt = modifiedAt;
	}
	
	public String getCreatedBy() {
	  return this.createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
	  this.createdBy = createdBy;
	}
	
	public String getModifiedBy() {
	  return this.modifiedBy;
	}
	
	public void setModifiedBy(String modifiedBy) {
	  this.modifiedBy = modifiedBy;
	}
	
	public Long getVersion() {
	  return this.version;
	}
	
	public void setVersion(Long version) {
	  this.version = version;
	}
	
	public String getRefid() {
	  return this.refid;
	}
	
	public void setRefid(String refid) {
	  this.refid = refid;
	}
	
	public String getEntityAlias() {
	  return this.entityAlias;
	}
	
	public void setEntityAlias(String entityAlias) {
	  this.entityAlias = entityAlias;
	}
	
	public String getEntityFqn() {
	  return this.entityFqn;
	}
	
	public void setEntityFqn(String entityFqn) {
	  this.entityFqn = entityFqn;
	}
}
