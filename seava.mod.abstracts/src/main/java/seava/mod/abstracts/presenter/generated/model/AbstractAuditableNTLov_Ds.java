/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.abstracts.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.presenter.model.AbstractDsModel;

public class AbstractAuditableNTLov_Ds<E> extends AbstractDsModel<E> implements IModelWithId<String> {
	
	public static final String ALIAS = "AbstractAuditableNTLov_Ds";
	
	public static final String f_id = "id";
	public static final String f_refid = "refid";
	
	@DsField
	private String id;
	
	@DsField
	private String refid;
	
	public AbstractAuditableNTLov_Ds() {
		super();
	}
	
	public AbstractAuditableNTLov_Ds(E e) {
		super(e);
	}
	
	public String getId() {
	  return this.id;
	}
	
	public void setId(String id) {
	  this.id = id;
	}
	
	public String getRefid() {
	  return this.refid;
	}
	
	public void setRefid(String refid) {
	  this.refid = refid;
	}
}
