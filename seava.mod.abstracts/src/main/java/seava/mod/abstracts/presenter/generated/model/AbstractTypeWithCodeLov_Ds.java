/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.abstracts.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.presenter.model.AbstractDsModel;

public class AbstractTypeWithCodeLov_Ds<E> extends AbstractDsModel<E> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "AbstractTypeWithCodeLov_Ds";
	
	public static final String f_id = "id";
	public static final String f_clientId = "clientId";
	public static final String f_code = "code";
	public static final String f_name = "name";
	public static final String f_active = "active";
	public static final String f_refid = "refid";
	
	@DsField
	private String id;
	
	@DsField
	private String clientId;
	
	@DsField
	private String code;
	
	@DsField
	private String name;
	
	@DsField
	private Boolean active;
	
	@DsField
	private String refid;
	
	public AbstractTypeWithCodeLov_Ds() {
		super();
	}
	
	public AbstractTypeWithCodeLov_Ds(E e) {
		super(e);
	}
	
	public String getId() {
	  return this.id;
	}
	
	public void setId(String id) {
	  this.id = id;
	}
	
	public String getClientId() {
	  return this.clientId;
	}
	
	public void setClientId(String clientId) {
	  this.clientId = clientId;
	}
	
	public String getCode() {
	  return this.code;
	}
	
	public void setCode(String code) {
	  this.code = code;
	}
	
	public String getName() {
	  return this.name;
	}
	
	public void setName(String name) {
	  this.name = name;
	}
	
	public Boolean getActive() {
	  return this.active;
	}
	
	public void setActive(Boolean active) {
	  this.active = active;
	}
	
	public String getRefid() {
	  return this.refid;
	}
	
	public void setRefid(String refid) {
	  this.refid = refid;
	}
}
