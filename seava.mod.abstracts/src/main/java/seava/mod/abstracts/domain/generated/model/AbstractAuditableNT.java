/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.abstracts.domain.generated.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import seava.mod.abstracts.domain.generated.model.AbstractBaseNT;
import seava.lib.j4e.api.base.session.Session;

@MappedSuperclass
public abstract class AbstractAuditableNT extends AbstractBaseNT {
  
  private static final long serialVersionUID = -8865917134914502125L;
      
  
  @Column(name="CREATED_AT", nullable=false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date createdAt;
  
  @Column(name="CREATED_BY", nullable=false)
  private String createdBy;
  
  @Column(name="MODIFIED_AT", nullable=false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date modifiedAt;
  
  @Column(name="MODIFIED_BY", nullable=false)
  private String modifiedBy;
  
  @Column(name="NOTES")
  private String notes;
  
  public Date getCreatedAt() {
    return this.createdAt;
  }
  
  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }
  
  public String getCreatedBy() {
    return this.createdBy;
  }
  
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }
  
  public Date getModifiedAt() {
    return this.modifiedAt;
  }
  
  public void setModifiedAt(Date modifiedAt) {
    this.modifiedAt = modifiedAt;
  }
  
  public String getModifiedBy() {
    return this.modifiedBy;
  }
  
  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }
  
  public String getNotes() {
    return this.notes;
  }
  
  public void setNotes(String notes) {
    this.notes = notes;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
    if (this.createdAt == null) {
      this.createdAt = new Date();
    }
    if (this.createdBy == null || this.createdBy.equals("")) {
      this.createdBy = Session.user.get().getCode();
    }
    if (this.modifiedAt == null) {
      this.modifiedAt = new Date();
    }
    if (this.modifiedBy == null || this.modifiedBy.equals("")) {
      this.modifiedBy = Session.user.get().getCode();
    }
  }
}
