/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.abstracts.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import seava.mod.abstracts.domain.generated.model.AbstractTypeNT;

@MappedSuperclass
public abstract class AbstractTypeWithCodeNT extends AbstractTypeNT {
  
  private static final long serialVersionUID = -8865917134914502125L;
      
  
  @Column(name="CODE", nullable=false)
  private String code;
  
  public String getCode() {
    return this.code;
  }
  
  public void setCode(String code) {
    this.code = code;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
