/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.abstracts.domain.generated.model;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractTypeWithCode;

@Entity(name=DummyTypeWithCode.ALIAS)
@Table(name=DummyTypeWithCode.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=DummyTypeWithCode.NQ_FIND_BY_CODE,
    query="SELECT e FROM "+DummyTypeWithCode.ALIAS+" e WHERE e.clientId = :clientId and e.code = :code",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  ),
  @NamedQuery(
    name=DummyTypeWithCode.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+DummyTypeWithCode.ALIAS+" e WHERE e.clientId = :clientId and e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
})
public class DummyTypeWithCode extends AbstractTypeWithCode {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "DummyTypeWithCode";
  
  public static final String TABLE_NAME = "DUMMY";
  /**
   * Named query find by unique key: Code.
   */
  public static final String NQ_FIND_BY_CODE = "DummyTypeWithCode.findByCode";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "DummyTypeWithCode.findByName";
      
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
