/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.abstracts.domain.generated.model;

import javax.persistence.MappedSuperclass;
import seava.mod.abstracts.domain.generated.model.AbstractEntity;

@MappedSuperclass
public abstract class AbstractBaseNT extends AbstractEntity {
  
  private static final long serialVersionUID = -8865917134914502125L;
      
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
