/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.abstracts.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import seava.mod.abstracts.domain.generated.model.AbstractAuditable;

@MappedSuperclass
public abstract class AbstractType extends AbstractAuditable {
  
  private static final long serialVersionUID = -8865917134914502125L;
      
  
  @Column(name="NAME", nullable=false)
  private String name;
  
  @Column(name="DESCRIPTION")
  private String description;
  
  @Column(name="ACTIVE", nullable=false)
  private Boolean active;
  
  public String getName() {
    return this.name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getDescription() {
    return this.description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }
  
  public Boolean getActive() {
    return this.active;
  }
  
  public void setActive(Boolean active) {
    this.active = active;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
    if (this.active == null) {
      this.active = false;
    }
  }
}
