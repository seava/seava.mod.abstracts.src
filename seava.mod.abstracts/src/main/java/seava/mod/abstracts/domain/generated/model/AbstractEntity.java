/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.abstracts.domain.generated.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import javax.persistence.Version;
import seava.lib.j4e.api.Constants;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import java.util.UUID;

@MappedSuperclass
public abstract class AbstractEntity implements Serializable, IModelWithId<String> {
  
  private static final long serialVersionUID = -8865917134914502125L;
      
  
  @Column(name="ID", nullable=false)
  @Id
  @GeneratedValue(generator = Constants.UUID_GENERATOR_NAME)
  private String id;
  
  @Column(name="REFID", nullable=false)
  private String refid;
  
  @Column(name="VERSION", nullable=false)
  @Version
  private Long version;
  
  @Transient
  private String entityAlias;
  
  @Transient
  private String entityFqn;
  
  public String getId() {
    return this.id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public String getRefid() {
    return this.refid;
  }
  
  public void setRefid(String refid) {
    this.refid = refid;
  }
  
  public Long getVersion() {
    return this.version;
  }
  
  public void setVersion(Long version) {
    this.version = version;
  }
  
  @Transient
  public String getEntityAlias() {
    return this.getClass().getSimpleName();
  }
  
  public void setEntityAlias(String entityAlias) {
    this.entityAlias = entityAlias;
  }
  
  @Transient
  public String getEntityFqn() {
    return this.getClass().getCanonicalName();
  }
  
  public void setEntityFqn(String entityFqn) {
    this.entityFqn = entityFqn;
  }
  
  @PrePersist
  public void preInsert()  {
     this.applyDefaultValues();
     this.preSave();
  }
  
  @PreUpdate
  public void preUpdate()  {
     this.applyDefaultValues();
     this.preSave();
  }
  
  public void preSave()  {
  }
  
  protected void applyDefaultValues() {
    if (this.refid == null || this.refid.equals("")) {
      this.refid = UUID.randomUUID().toString().toUpperCase();
    }
  }
}
