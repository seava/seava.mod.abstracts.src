/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.abstracts.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.mod.abstracts.domain.generated.model.AbstractEntity;
import seava.lib.j4e.api.base.session.Session;

@MappedSuperclass
public abstract class AbstractBase extends AbstractEntity implements IModelWithClientId {
  
  private static final long serialVersionUID = -8865917134914502125L;
      
  
  @Column(name="CLIENTID", nullable=false)
  private String clientId;
  
  public String getClientId() {
    return this.clientId;
  }
  
  public void setClientId(String clientId) {
    this.clientId = clientId;
  }
  
  public void preSave()  {
     this.__validate_client_context__();
  }
  
  public void __validate_client_context__()  {
     if (this.clientId != null
         && !Session.user.get().getClient().getId().equals(this.clientId)) {
       throw new RuntimeException(
           "Client conflict detected. You are trying to work with an entity which belongs to client with id=`"
               + this.clientId
               + "` but the current session is connected to client with id=`"
               + Session.user.get().getClient().getId() + "` ");
     }
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
    if (this.clientId == null || this.clientId.equals("")) {
      this.clientId = Session.user.get().getClientId();
    }
  }
}
